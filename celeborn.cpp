#include "./index.hpp"
#include "./parser.hpp"
#include <chrono>
#include <fstream>
#include <iostream>
#include <vector>

using namespace std::chrono;

int main(int argc, char *argv[]) {
  std::string fname = argv[1];
  std::string index_name = argv[2];
  std::string index_stats = argv[3];
  std::vector<Doc> docs;

  auto start = high_resolution_clock::now();
  parse(fname, docs);
  auto stop = high_resolution_clock::now();
  auto duration = duration_cast<microseconds>(stop - start);
  //std::cout << "Parsing (seconds): " << duration.count() / 1e6 << std::endl;

  // start = high_resolution_clock::now();
  // clean(docs);
  // stop = high_resolution_clock::now();
  // duration = duration_cast<microseconds>(stop - start);
  // std::cout << "Cleaning (microseconds): " << duration.count() << std::endl;

  Index i;
  start = high_resolution_clock::now();
  for (auto &doc : docs) {
    i.index_doc(doc);
  }
  stop = high_resolution_clock::now();
  duration = duration_cast<microseconds>(stop - start);
  //std::cout << "Indexing (seconds): " << duration.count() / 1e6 << std::endl;
  i.display();
  start = high_resolution_clock::now();
  i.write_index(index_name);
  stop = high_resolution_clock::now();
  duration = duration_cast<microseconds>(stop - start);
  //std::cout << "Writing (seconds): " << duration.count() / 1e6 << std::endl;

  std::ofstream file(argv[3]);
  file << i.total << '\n';
  file << i.vocab.size() << '\n';
}
