#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <re2/re2.h>
#include <set>
#include <string>
#include <vector>
#include "libstemmer.h"

const std::set<std::string> STOPWORDS = {
    "i",          "me",        "my",      "myself", "we",         "our",
    "ours",       "ourselves", "you",     "your",   "yours",      "yourself",
    "yourselves", "he",        "him",     "his",    "himself",    "she",
    "her",        "hers",      "herself", "it",     "its",        "itself",
    "they",       "them",      "their",   "theirs", "themselves", "what",
    "which",      "who",       "whom",    "this",   "that",       "these",
    "those",      "am",        "is",      "are",    "was",        "were",
    "be",         "been",      "being",   "have",   "has",        "had",
    "having",     "do",        "does",    "did",    "doing",      "a",
    "an",         "the",       "and",     "but",    "if",         "or",
    "because",    "as",        "until",   "while",  "of",         "at",
    "by",         "for",       "with",    "about",  "against",    "between",
    "into",       "through",   "during",  "before", "after",      "above",
    "below",      "to",        "from",    "up",     "down",       "in",
    "out",        "on",        "off",     "over",   "under",      "again",
    "further",    "then",      "once",    "here",   "there",      "when",
    "where",      "why",       "how",     "all",    "any",        "both",
    "each",       "few",       "more",    "most",   "other",      "some",
    "such",       "no",        "nor",     "not",    "only",       "own",
    "same",       "so",        "than",    "too",    "very",       "s",
    "t",          "can",       "will",    "just",   "don",        "should",
    "now"};

const RE2 RE_SEP("([\\S]+)([\\s]+)?");
const RE2 RE_TOKEN("([a-zA-Z\\-]*)(?:[\\.])?");
// std::regex TOKEN("[^\\s]+");

// const std::regex PROTOCOL("https?:\/\/|ftp:\/\/");
// const std::regex TLDS("\S*\.(com|org|net|int|edu|gov|mil|uk|ro|pl|)\S*");
const RE2 RE_CAT("\\[\\[[Cc]ategory:(.*)\\]\\]");
const RE2 RE_REF("==\\s*references\\s*==\\n*((?:^\\*.*\\n*)*)");
// std::regex_constants::ECMAScript); const std::regex
// RINF("(?=\{infobox)(\{([^{}]|(?1))*\})");

const std::string charlist =
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567"
    "89!@#$%^&*()-+=[]{};:<>?/|,.~`";

// For ~6GB indexes
// const unsigned long long BLOCK_SIZE = 17 * 1000000000;
const unsigned long long BLOCK_SIZE = 3 * 10000000;

#endif
