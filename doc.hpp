#ifndef DOC_H
#define DOC_H

#include <string>

typedef unsigned long long doc_id;

class Doc {
public:
  doc_id id;
  std::string title, body;

  Doc(doc_id id, std::string title, std::string body) {
    this->id = id;
    this->title = title;
    this->body = body;
  }

  Doc(doc_id id) {
    this->id = id;
    this->title = "";
    this->body = "";
  }
};

#endif
