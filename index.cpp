#include "./index.hpp"
#include "./constants.hpp"
#include "./doc.hpp"
#include "./utils.hpp"
#include <cassert>
#include <iostream>
#include <re2/re2.h>
#include <string>
#include <vector>

bool alphanumericstring(const std::string &str) {
  return find_if(str.begin(), str.end(), [](char c) {
           return !(isalnum(c) || (c == ' '));
         }) == str.end();
}

std::string encode_int(int x) {
  std::string s;
  x++;
  while (x) {
    s += charlist[x % charlist.size()];
    x /= charlist.size();
  }
  return s;
}

int decode_int(std::string x) {
  int r = 0;
  for (int i = (int)x.size() - 1; i >= 0; i--) {
    auto tmp =
        std::find(charlist.begin(), charlist.end(), x[i]) - charlist.begin();
    r = r * charlist.size() + tmp;
  }
  r--;
  return r;
}

void Index::insert(const std::string s, const Posting p) {
  if (!exists(s)) {
    vocab.insert({s, vocab.size()});
  }
  idx[vocab[s]].insert(p);
  posting_count++;
}

void Index::index_plaintext(std::string &text, doc_id &d, Location l) {
    re2::StringPiece input(text);
    string raw;
    while(RE2::Consume(&input, RE_SEP, &raw)) {
        string token;
        if(RE2::FullMatch(raw, RE_TOKEN, &token)) {
            lower(token);
            token = stem(token);
            if (token.length() < 3 || STOPWORDS.find(token) != STOPWORDS.end()) {
                total++;
                continue;
            }
            add_location(token, d, l);
            add1_freq(token, d, l);
        }
    }
}

void Index::index_references(Doc &d) {
    std::string ignored, body;
    re2::StringPiece input(d.body);
    RE2::Consume(&input, RE_REF, &ignored, &body);
    index_plaintext(body, d.id, REFERENCES);
}

void Index::index_category(Doc &d) {
    //index_plaintext(s, d.id, CATEGORY);
}

void Index::index_links(Doc &d) {}

void Index::index_title(Doc &d) { index_plaintext(d.title, d.id, TITLE); }

void Index::index_body(Doc &d) { index_plaintext(d.body, d.id, BODY); }

bool Index::index_doc(Doc d) {
  index_references(d);
  index_category(d);
  index_links(d);
  index_title(d);
  index_body(d);
  return sizeof(Posting) * posting_count >= BLOCK_SIZE;
}

void Index::display() {
  std::cout << "Vocab:\n";
  for (auto tp : vocab) {
    std::cout << tp.first << ", " << tp.second << std::endl;
  }
  for (auto tp : idx) {
    std::cout << tp.first << ":\n";
    for (auto p : tp.second) {
      std::cout << "\t" << p.stringify() << std::endl;
    }
  }
}

void Index::add_location(std::string s, doc_id d, Location l) {
  if (exists(s)) {
    if (idx[vocab[s]].find({d}) == idx[vocab[s]].end()) {
      idx[vocab[s]].insert({d, 1, l});
      posting_count++;
    } else {
      auto &p = *(idx[vocab[s]]).find({d});
      p.l = p.l | l;
    }
  } else {
    insert(s, {d, 1, l});
      posting_count++;
  }
}

void Index::add1_freq(std::string s, doc_id d, Location l) {
  if (exists(s)) {
    if (idx[vocab[s]].find({d}) == idx[vocab[s]].end()) {
      idx[vocab[s]].insert({d, 1, l});
      posting_count++;
    } else {
      auto &p = *(idx[vocab[s]]).find({d});
      p.frequency++;
    }
  } else {
    insert(s, {d, 1, l});
      posting_count++;
  }
}

void Index::write_index(string fname) {
  ofstream file(fname);
  auto vsz = vocab.size();
  file << encode_int(vsz) << '\n';
  vector<string> terms(vsz);
  for (auto &p : vocab) {
    assert(p.second < vsz);
    terms[p.second] = p.first;
  }
  for (auto x : terms) {
    file << x << '\n';
  }
  // file << encode_int(index.idx.size()) << '\n';
  for (int i = 0; i < vsz; i++) {
    // print set size
    for (auto &post : idx[i]) {
      file << encode_int(post.d) << ' ' << encode_int(post.frequency) << ' '
           << encode_int(post.l) << '\n';
    }
    file << '\n';
  }
}

void Index::flush_to_disk(std::string fname) {
    ofstream file(fname);
    for(auto& p : idx) {
        file << p.first << '\n';
        for(auto& posting : p.second) {
            file << posting.d << ' ' << posting.frequency << ' ' << posting.l << '\n';
        }
        file << '\n';
    }
    idx = {};
    posting_count = 0;
}

void Index::read_index(string fname) {
  ifstream file(fname);
  string tmp;
  file >> tmp;
  int vsz = decode_int(tmp);
  vector<string> terms(vsz);
  for (int i = 0; i < vsz; i++) {
    file >> tmp;
    vocab.insert({tmp, vocab.size()});
    terms[i] = tmp;
  }
  for (int i = 0; i < vsz; i++) {
    string s;
    while (getline(file, s)) {
      if (s.empty()) {
        goto NEXT;
      } else {
        std::istringstream ss(s);
        string d, f, l;
        ss >> d;
        ss >> f;
        ss >> l;
        insert(terms[i],
               {(doc_id)decode_int(d), (unsigned long long)decode_int(f),
                (Location)decode_int(l)});
      }
    }
  NEXT:;
  }
}
