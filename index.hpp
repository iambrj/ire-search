#ifndef INDEX_H
#define INDEX_H

#include "./doc.hpp"
#include "./posting.hpp"
#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <sstream>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <vector>

using std::ifstream;
using std::ofstream;
using std::string;
using std::vector;

typedef unsigned long long tid;

class Index {
private:
    unsigned long long posting_count = 0;
  void index_plaintext(std::string &text, doc_id &d, Location l);

  void index_references(Doc &d);

  void index_category(Doc &d);

  void index_links(Doc &d);

  void index_title(Doc &d);

  void index_body(Doc &d);

public:
  std::unordered_map<std::string, tid> vocab;
  std::map<tid, std::unordered_set<Posting>> idx;
  unsigned long long total = 0;

  void insert(const std::string s, const Posting p);

  bool exists(const std::string &s) { return vocab.find(s) != vocab.end(); }

  void add1_freq(std::string s, doc_id d, Location l);

  void add_location(std::string s, doc_id d, Location l);

  void display();

  bool index_doc(Doc d);
  void read_index(string fname);
  void write_index(string fname);
  void flush_to_disk(string fname);
};

#endif
