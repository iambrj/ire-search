#include <iostream>
#include <fstream>
#include <map>
#include <unordered_set>
#include <string>
#include <vector>
#include <sstream>
#include "merge.hpp"

using std::string;
using std::vector;
using std::ifstream;
using std::ofstream;
using std::getline;

void merge(int dump_count, char *dump_names[], string fname, unsigned long long merge_limit) {
    ofstream file(fname);
    vector<ifstream> dumps;
    for(int i = 0; i < dump_count; i++) {
        std::ifstream tmp(dump_names[i]);
        dumps.push_back(tmp);
    }
    std::map<unsigned long long, std::unordered_set<std::ifstream>> tid_stream;
    for(auto& x : dumps) {
        string s;
        while (getline(x, s)) {
            if (s.empty()) {
                goto NEXT1;
            } else {
                std::istringstream ss(s);
                unsigned long long tid;
                ss >> tid;
                tid_stream[tid].insert(x);
            }
        }
        NEXT1:;
    }
    while(!tid_stream.empty()) {
        auto tid = (*tid_stream.begin()).first;
        auto streamset = tid_stream[tid];
        file << tid << '\n';
        for(auto& stream : streamset) {
            string s;
            while (getline(stream, s)) {
                if (s.empty()) {
                    goto NEXT2;
                } else {
                    std::istringstream ss(s);
                    unsigned long long tid;
                    ss >> tid;
                    tid_stream[tid].insert(x);
                }
            }
        NEXT2:;
        }
    }
}
