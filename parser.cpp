#include "./parser.hpp"
#include "./doc.hpp"
#include <cstring>
#include <libxml/xmlreader.h>
#include <string>
#include <vector>

void processNode(xmlTextReaderPtr &reader, std::vector<Doc> &docs) {
  xmlChar *name, *value;

  name = xmlTextReaderName(reader);
  if (name == NULL)
    name = xmlStrdup(BAD_CAST "--");
  value = xmlTextReaderValue(reader);

  if (xmlTextReaderNodeType(reader) == 1) {
    if (strcmp((char *)name, "title") == 0) {
      xmlFree(name);
      xmlFree(value);
      xmlTextReaderRead(reader);
      name = xmlTextReaderName(reader);
      value = xmlTextReaderValue(reader);
      Doc d(docs.size(), (char *)value, "");
      docs.push_back(d);
    } else if (strcmp((char *)name, "text") == 0) {
      xmlFree(name);
      xmlFree(value);
      xmlTextReaderRead(reader);
      name = xmlTextReaderName(reader);
      value = xmlTextReaderValue(reader);
      docs[docs.size() - 1].body = (char *)value;
    }
  }
  xmlFree(name);
  if (value != NULL) {
    xmlFree(value);
  }
}

void parse(std::string filename, std::vector<Doc> &docs) {
  xmlTextReaderPtr reader;
  int ret;

  reader = xmlNewTextReaderFilename(filename.c_str());
  if (reader != NULL) {
    ret = xmlTextReaderRead(reader);
    while (ret == 1) {
      processNode(reader, docs);
      ret = xmlTextReaderRead(reader);
    }
    xmlFreeTextReader(reader);
    if (ret != 0) {
      printf("%s : failed to parse\n", filename.c_str());
    }
  } else {
    printf("Unable to open %s\n", filename.c_str());
  }
}
