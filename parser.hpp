#ifndef PARSER_H
#define PARSER_H

#include "./doc.hpp"
#include <string>
#include <vector>

void parse(std::string filename, std::vector<Doc> &docs);

#endif
