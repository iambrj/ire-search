#ifndef POSTING_H
#define POSTING_H

#include "./doc.hpp"
#include <string>
#include <unordered_map>

typedef unsigned int Location;

const Location TITLE = {1};
const Location INFOBOX = {1 << 1};
const Location BODY = {1 << 2};
const Location CATEGORY = {1 << 3};
const Location LINKS = {1 << 4};
const Location REFERENCES = {1 << 5};

class Posting {
public:
  doc_id d;
  mutable unsigned long long frequency;
  mutable Location l;

  void add_location(Location l) { this->l = this->l | l; }

  bool operator==(const Posting &another) const {
    // INVARIANT: each (term, doc) has unique posting
    return this->d == another.d;
    // and this->frequency == another.frequency and
    // this->l == another.l;
  }

  std::string stringify() {
    return "doc_id: " + std::to_string(d) +
           ", frequency: " + std::to_string(frequency) +
           ", location mask: " + std::to_string(l);
  }
};

namespace std {
template <> struct hash<Posting> {
  size_t operator()(const Posting &p) const {
    // Compute individual hash values for two data members and combine them
    // using XOR and bit shifting
    return hash<doc_id>{}(p.d);
  }
};
} // namespace std

#endif
