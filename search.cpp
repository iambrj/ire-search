#include <algorithm>
#include <map>
#include <regex>
#include <string>
#include <unordered_set>

#include "constants.hpp"
#include "index.hpp"
#include "posting.hpp"
#include "serialize.hpp"
#include "utils.hpp"

const std::string FIELDS = "bcilrt";

void trim(std::string &s) { s.erase(s.find_last_not_of(" \n\r\t") + 1); }

std::map<char, std::string> parse_query(std::string q) {
  std::map<char, std::string> m;
  char tag = 'b';
  std::string run;
  for (int i = 0; i < (int)q.size(); i++) {
    if (i + 1 < q.size() && q[i + 1] == ':') {
      trim(run);
      if (run != "") {
        m[tag] = run;
      }
      tag = q[i];
      run = "";
      i++;
      continue;
    }
    run += q[i];
  }
  trim(run);
  if (run != "")
    m[tag] = run;
  return m;
}
// "t:World Cup i:2018 c:Football"
int main(int argc, char *argv[]) {
  // auto query = parse_query("this is some body c:Football t:World Cup
  // i:2018");
  auto query = parse_query(argv[2]);
  Index idx;
  read_index(idx, argv[1]);
  for (auto &p : query) {
    std::vector<std::string> tokens;
    auto text = p.second;
    for (std::sregex_iterator i =
             std::sregex_iterator(text.begin(), text.end(), RE_TOKEN);
         i != std::sregex_iterator(); i++) {
      std::smatch m = *i;
      std::string token = m.str();
      // if(token.length() > 1 && token[token.length() - 1] == '.')
      // token.pop_back(); if(token == "" || !alphanumericstring(token))
      // continue; Case folding
      lower(token);
      // Stemming
      // struct sb_stemmer* stemmer = sb_stemmer_new("english", NULL);
      // const sb_symbol* b = (sb_symbol *) token.c_str();
      // const sb_symbol* stemmed = sb_stemmer_stem(stemmer, b, strlen((char *)
      // b)); token = (char *) stemmed;
      if (token.length() < 4 || STOPWORDS.find(token) != STOPWORDS.end())
        continue;
      tokens.push_back(token);
    }

    // for (auto p : tokens) {
    //     std::cout << p << '\n';
    // }
    std::cout << "{";
    for (int i = 0; i < tokens.size(); i++) {
      auto token = tokens[i];
      if (!idx.v.exists(token)) {
        std::cout << "\"" << token
                  << "\": { \"title\":[], \"body\":[], \"infobox\":[], "
                     "\"categories\":[], \"references\":[], \"links\":[]}";
        if (i + 1 != tokens.size())
          std::cout << ",";
        std::cout << std::endl;
      } else {
        auto tid = idx.v[token];
        std::vector<Posting> v;
        for (auto x : idx.idx[tid]) {
          v.push_back(x);
        }
        std::map<std::string, std::vector<int>> m;
        for (int j = 0; j < v.size(); j++) {
          if (TITLE & v[j].l) {
            m["title"].push_back(v[j].d);
          }
          if (INFOBOX & v[j].l) {
            m["infobox"].push_back(v[j].d);
          }
          if (BODY & v[j].l) {
            m["body"].push_back(v[j].d);
          }
          if (CATEGORY & v[j].l) {
            m["category"].push_back(v[j].d);
          }
          if (LINKS & v[j].l) {
            m["links"].push_back(v[j].d);
          }
          if (REFERENCES & v[j].l) {
            m["references"].push_back(v[j].d);
          }
        }
        std::cout << "\"" << token << "\": { \"title\":[";
        for (int j = 0; j < m["title"].size(); j++) {
          std::cout << m["title"][j];
          if (j + 1 != m["title"].size())
            std::cout << ",";
        }
        std::cout << "], \"body\":[";
        for (int j = 0; j < m["body"].size(); j++) {
          std::cout << m["body"][j];
          if (j + 1 != m["body"].size())
            std::cout << ",";
        }
        std::cout << "], \"infobox\":[";
        for (int j = 0; j < m["infobox"].size(); j++) {
          std::cout << m["infobox"][j];
          if (j + 1 != m["infobox"].size())
            std::cout << ",";
        }
        std::cout << "], \"categories\":[";
        for (int j = 0; j < m["categories"].size(); j++) {
          std::cout << m["categories"][j];
          if (j + 1 != m["categories"].size())
            std::cout << ",";
        }
        std::cout << "], \"references\":[";
        for (int j = 0; j < m["references"].size(); j++) {
          std::cout << m["references"][j];
          if (j + 1 != m["references"].size())
            std::cout << ",";
        }
        std::cout << "], \"links\":[";
        for (int j = 0; j < m["links"].size(); j++) {
          std::cout << m["links"][j];
          if (j + 1 != m["links"].size())
            std::cout << ",";
        }
        std::cout << "]}";
        if (i + 1 != tokens.size())
          std::cout << ",";
      }
    }
    std::cout << "}";
  }
}
/*
{
"world": {
        "title": [129, 130, 480, 513, ... ],
        "body": [58, 83, 84, 88, ... ],
        "infobox": [ ... ],
        "categories": [ ... ],
        "references": [ ... ],
        "links": [ ... ]
           },
"cup": {
    .
    .
    .
    },
.
.
.
}
*/
