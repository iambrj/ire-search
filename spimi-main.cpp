#include <iostream>
#include <string>
#include "spimi.hpp"

int main(int argc, char *argv[]) {
    if(argc != 4) {
        std::cerr << "Error! Usage: a.out <dump_file> <indexes_path> <stats_path>" << std::endl;
    }
    std::string dump_file = argv[1], indexes_path = argv[2], stats_path = argv[3];
    spimi(dump_file, indexes_path);
}
