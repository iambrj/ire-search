#include <cstring>
#include <libxml/xmlreader.h>
#include <string>
#include "spimi.hpp"
#include "doc.hpp"
#include "index.hpp"

bool processNode(xmlTextReaderPtr &reader, Doc& d, unsigned long long& new_id, Index& idx) {
  xmlChar *name, *value;
  bool ret = false;

  name = xmlTextReaderName(reader);
  if (name == NULL)
    name = xmlStrdup(BAD_CAST "--");
  value = xmlTextReaderValue(reader);

  if (xmlTextReaderNodeType(reader) == 1) {
    if (strcmp((char *)name, "title") == 0) {
      xmlFree(name);
      xmlFree(value);
      xmlTextReaderRead(reader);
      name = xmlTextReaderName(reader);
      value = xmlTextReaderValue(reader);
      d.id = new_id++;
      d.title = (char *) value;
    } else if (strcmp((char *)name, "text") == 0) {
      xmlFree(name);
      xmlFree(value);
      xmlTextReaderRead(reader);
      name = xmlTextReaderName(reader);
      value = xmlTextReaderValue(reader);
      d.body = (char *) value;
      ret = idx.index_doc(d);
    }
  }
  xmlFree(name);
  if (value != NULL) {
    xmlFree(value);
  }
  return ret;
}

void spimi(std::string dump_file, std::string indexes_path) {
  xmlTextReaderPtr reader;
  int ret;
  Index idx;
  doc_id new_id = 0;
  unsigned int flushes = 0;
  Doc d({new_id});

  reader = xmlNewTextReaderFilename(dump_file.c_str());
  if (reader != NULL) {
    ret = xmlTextReaderRead(reader);
    while (ret == 1) {
        if(processNode(reader, d, new_id, idx)) {
            idx.flush_to_disk(indexes_path + std::to_string(flushes++));
        }
      ret = xmlTextReaderRead(reader);
    }
    xmlFreeTextReader(reader);
    if (ret != 0) {
      printf("%s : failed to parse\n", dump_file.c_str());
    }
  } else {
    printf("Unable to open %s\n", dump_file.c_str());
  }
  idx.write_index(indexes_path);
}
