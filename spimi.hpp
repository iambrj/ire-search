#ifndef PARSER_H
#define PARSER_H

#include <libxml/xmlreader.h>
#include <string>
#include "doc.hpp"
#include "index.hpp"

int processNode(xmlTextReaderPtr &reader);
void spimi(std::string dump_file, std::string indexes_path);

#endif
