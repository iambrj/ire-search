#include "./utils.hpp"
#include "./constants.hpp"
#include "libstemmer.h"
#include <algorithm>
#include <string>

struct sb_stemmer* STEMMER = sb_stemmer_new("english", NULL);

void lower(std::string &s) {
  std::transform(s.begin(), s.end(), s.begin(),
                 [](unsigned char c) { return std::tolower(c); });
}

std::string stem(std::string &token) {
    const sb_symbol* b = (const sb_symbol *) token.c_str();
    const sb_symbol* stemmed = sb_stemmer_stem(STEMMER, b, strlen((char *) b));
    return (char *) stemmed;
}
