#ifndef UTILS_H
#define UTILS_H

#include <algorithm>
#include <string>

void lower(std::string &s);
std::string stem(std::string &s);

#endif
